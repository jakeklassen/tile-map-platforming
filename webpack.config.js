const path = require('path');

module.exports = {
	devtool: 'source-map',
	entry: path.resolve(__dirname, 'src/game.js'),
	output: {
		path: __dirname,
		filename: "./build/bundle.js"
	},

	module: {
		loaders: [
			{
				test: /\.json$/,
				loader: 'json'
			},
			{
				test: /\.js$/,
				exclude: /(node_modules|bower_components)/,
				include: path.join(__dirname, 'src'),
				loader: 'babel',
				query: {
					presets: ['es2015-node6', 'stage-2']
				}
			}
		]
	}
};