//================================================
// Utilities
//================================================

// High resolution timer if supported or fallback to Date.
// Milliseconds
function timestamp() {
	return window.performance && window.performance.now ? window.performance.now() : new Date().getTime();
}

//================================================
// Variables
//================================================

const	minJumpHeight 		= 1;
const maxJumpHeight			= 3.5;
const timeToJumpMin   	= 0.2;
const timeToJumpApex  	= 0.4;
const jumpCooldown      = 0.15;
const maxFallSpeed			= 30;
const gravity 					= (2 * maxJumpHeight) / Math.pow(timeToJumpApex, 2);
const jumpVelocity    	= -Math.abs(gravity) * timeToJumpApex;
const fps         			= 60;
const step        			= 1 / fps;
const canvas      			= document.getElementById('game');
const ctx         			= canvas.getContext('2d');
const WIDTH       			= canvas.width;
const HEIGHT      			= canvas.height;
const TILE_WIDTH  			= 32;
const TILE_HEIGHT 			= 32;
const MAP_WIDTH   			= WIDTH / TILE_WIDTH;
const MAP_HEIGHT  			= HEIGHT / TILE_HEIGHT;
const KEY         			= { Z: 90, SPACE: 32, LEFT: 37, UP: 38, RIGHT: 39, DOWN: 40 };
const TILES							= { EMPTY: 0, BLOCK: 1, LEFT_SLOPE: 2, RIGHT_SLOPE: 3 };
let isPaused      			= false;

canvas.style.background = '#000000';
ctx.imageSmoothingEnabled = false;
ctx.mozImageSmoothingEnabled = false;

const player = {
	pos: { x: 1, y: 10 },
	bounds: {
		width: 32 / TILE_WIDTH,
		height: 32 / TILE_HEIGHT,
		left() {
			return player.pos.x;
		},
		right() {
			return player.pos.x + player.bounds.width;
		},
		top() {
			return player.pos.y;
		},
		bottom() {
			return player.pos.y + player.bounds.height;
		}
	},
	input: { up: false, down: false, left: false, right: false, jump: false },
	collisions: { above: false, below: false, left: false, right: false },
	airborne: false,
	dx: 0,
	dy: 0,
	vx: 0,
	vy: 0,
	speed: 10,
	timers: {
		jump: 0
	},
	lastFrame: {
		collisions: { above: false, below: false, left: false, right: false }
	}
};

// 0 - Empty
// 1 - Filled
// 2 - \ Angle
// 3 = / Angle
const map = [
	[2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 3, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
	[1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1],
	[1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
	[1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
	[1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1],
	[1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1],
	[1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1],
	[1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
];

// Keyboard listener
function onkey(ev, key, down) {
	switch (key) {
		case KEY.LEFT:
			player.input.left = down;
			ev.preventDefault();
			return false;

		case KEY.RIGHT:
			player.input.right = down;
			ev.preventDefault();
			return false;

		case KEY.UP:
			//player.input.up = down;
			ev.preventDefault();
			return false;

		case KEY.DOWN:
			//player.input.down = down;
			ev.preventDefault();
			return false;

		case KEY.SPACE:
			player.input.jump = down;
			ev.preventDefault();
			return false;
	}
}

function updatePlayer(dt) {
	player.dx = 0;
	player.timers.jump += dt;

	// Prepare velocities for collision checking
	if (player.collisions.above || player.collisions.below) {
		player.vy = 0;
	}

	if (player.input.left) {
		player.dx = -1;
	}

	if (player.input.right) {
		player.dx = 1;
	}

	if (player.timers.jump >= jumpCooldown &&
			player.input.jump &&
			player.collisions.below &&
			!player.collisions.above &&
			!player.airborne) {
		player.timers.jump = 0;
		player.airborne = true;
		player.vy = jumpVelocity;
	}

	if (player.airborne && player.collisions.below) {
		player.airborne = false;
	}

	let vx = player.dx;
	if (player.dx === 1 && player.collisions.right) {
		vx = 0;
	} else if (player.dx === -1 && player.collisions.left) {
		vx = 0;
	}

	player.vx = vx * player.speed;
	player.vy += gravity * dt;

	if (player.vy > maxFallSpeed) {
		player.vy = maxFallSpeed;
	}

	// Reset attributes - track last frame for some comparisons
	player.lastFrame.collisions.left = player.collisions.left;
	player.lastFrame.collisions.right = player.collisions.right;
	player.lastFrame.collisions.above = player.collisions.above;
	player.lastFrame.collisions.below = player.collisions.below;

	player.collisions.left = false;
	player.collisions.right = false;
	player.collisions.above = false;
	player.collisions.below = false;

	// Store new x position after adding velocity
	const newX = player.pos.x + player.vx * dt;

	// Get tiles on either side of player
	const tileLeft = Math.floor(player.pos.x) - 1;
	const tileRight = Math.ceil(player.pos.x) + 1;

	if (map[Math.floor(player.pos.y)][tileLeft] !== TILES.EMPTY ||
			map[Math.ceil(player.pos.y)][tileLeft] !== TILES.EMPTY) {
		if (player.vx < 0 && newX < tileLeft + 1) {
			player.pos.x = tileLeft + 1;
			player.vx = 0;
			player.collisions.left = true;
		}
	}

	if (map[Math.floor(player.pos.y)][tileRight] !== TILES.EMPTY ||
			map[Math.ceil(player.pos.y)][tileRight] !== TILES.EMPTY) {
		if (player.vx > 0 && newX + 1 > tileRight) {
			player.pos.x = tileRight - 1;
			player.vx = 0;
			player.collisions.right = true;
		}
	}

	// Move x
	player.pos.x += player.vx * dt;

	// Store new y position after adding velocity
	const newY = player.pos.y + player.vy * dt;

	// Get tiles above/below player
	const tileAbove = Math.floor(player.pos.y) - 1;
	const tileBelow = Math.ceil(player.pos.y) + 1;

	// Look below player
	if (map[tileBelow][Math.floor(player.pos.x)] !== TILES.EMPTY ||
			map[tileBelow][Math.ceil(player.pos.x)] !== TILES.EMPTY) {
		// Falling
		if (player.vy > 0 && newY + 1 > tileBelow) {
			player.pos.y = tileBelow - 1;
			player.vy = 0;
			player.collisions.below = true;
		}
	}

	// Look above player
	if (map[tileAbove][Math.floor(player.pos.x)] !== TILES.EMPTY ||
			map[tileAbove][Math.ceil(player.pos.x)] !== TILES.EMPTY) {
		// Jumping
		if (player.vy < 0 && newY < tileAbove + 1) {
			player.pos.y = tileAbove + 1;
			player.vy = 0;
			player.collisions.above = true;
		}
	}

	// Move y
	player.pos.y += player.vy * dt;
}

function update(dt) {
	updatePlayer(dt);
}

function render() {
	ctx.clearRect(0, 0, canvas.width, canvas.height);

	ctx.fillStyle = 'green';
	for (let y = 0; y < MAP_HEIGHT; ++y) {
		for (let x = 0; x < MAP_WIDTH; ++x) {
			if (map[y][x] === TILES.BLOCK) {
				ctx.fillRect(x * TILE_WIDTH, y * TILE_HEIGHT, TILE_WIDTH, TILE_HEIGHT);
			} else if (map[y][x] === TILES.LEFT_SLOPE) {
				// 45 degree right slope
				ctx.beginPath();
				ctx.moveTo(x * TILE_WIDTH, y * TILE_HEIGHT + TILE_HEIGHT);
				ctx.lineTo(x * TILE_WIDTH + TILE_WIDTH, y * TILE_HEIGHT + TILE_HEIGHT);
				ctx.lineTo(x * TILE_WIDTH, y * TILE_HEIGHT );
				ctx.fill();
			} else if (map[y][x] === TILES.RIGHT_SLOPE) {
				// 45 degree left slope
				ctx.beginPath();
				ctx.moveTo(x * TILE_WIDTH, y * TILE_HEIGHT + TILE_HEIGHT);
				ctx.lineTo(x * TILE_WIDTH + TILE_WIDTH, y * TILE_HEIGHT + TILE_HEIGHT);
				ctx.lineTo(x * TILE_WIDTH + TILE_WIDTH, y * TILE_HEIGHT);
				ctx.fill();
			}
		}
	}

	ctx.fillStyle = 'white';

	ctx.fillRect(
		player.pos.x * TILE_WIDTH,
		player.pos.y * TILE_HEIGHT,
		player.bounds.width * TILE_WIDTH,
		player.bounds.height * TILE_HEIGHT
	);
}

//================================================
// The Game Loop
//================================================

let dt = 0;
let now;
let last = timestamp();

// Fixed rate loop
function frame() {
	now = timestamp();
	// One additional note is that requestAnimationFrame might pause if our browser
	// loses focus, resulting in a very, very large dt after it resumes.
	// We can workaround this by limiting the delta to one second:
	dt = dt + Math.min(1, (now - last) / 1000);
	while (dt > step) {
		dt = dt - step;
		update(step);
	}

	if (isPaused === false) {
		render();
	} else {
		render(0);
	}

	last = now;

	window.requestAnimationFrame(frame);
}

// Add events
document.addEventListener('keydown', function(ev) { return onkey(ev, ev.keyCode, true); }, false);
document.addEventListener('keyup', function(ev) { return onkey(ev, ev.keyCode, false); }, false);

// Init game loop
frame();
